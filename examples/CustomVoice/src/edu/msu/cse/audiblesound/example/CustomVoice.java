package edu.msu.cse.audiblesound.example;

import edu.msu.cse.audible.sound.Audible;
import edu.msu.cse.audible.sound.Voice;

/**
 * This is the most basic possible custom voice.
 */
public class CustomVoice extends Voice {
    private float frequency = 1000;
    
    private float amplitude = 0.5f;
    
    // This must be a double to avoid precision issues
    private double time = 0;
    
    public enum Waveform {Sine, Square, Saw};
    
    private Waveform waveform = Waveform.Sine;
    
    public CustomVoice(Audible audible) {
        super(audible);
    }
    
    @Override
    synchronized public void update(float delta) {
        time += delta;
        
        float sample = 0;
        
        switch(waveform) {
            case Sine:
                sample = (float)Math.sin(2 * Math.PI * frequency * time);
                break;
                
            case Square:
                sample = (frequency * time % 1.0) < 0.5 ? 1 : -1;
                break;
                
            case Saw:
                sample = (float)((frequency * time % 1.0) * 2 - 1);
                break;
        }
        
        sample *= amplitude;
        
        audio[0] = sample;
        audio[1] = sample;
    }
    
    /**
     * @return the frequency
     */
    public float getFrequency() {
        return frequency;
    }

    /**
     * @param frequency the frequency to set
     */
    public void setFrequency(float frequency) {
        this.frequency = frequency;
    }
    
    /**
     * @return the amplitude
     */
    public float getAmplitude() {
        return amplitude;
    }

    /**
     * @param amplitude the amplitude to set
     */
    public void setAmplitude(float amplitude) {
        this.amplitude = amplitude;
    }

    public Waveform getWaveform() {
        return waveform;
    }

    public void setWaveform(Waveform waveform) {
        this.waveform = waveform;
    }
}
