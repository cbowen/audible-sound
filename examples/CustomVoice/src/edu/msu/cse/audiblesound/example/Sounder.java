package edu.msu.cse.audiblesound.example;

import edu.msu.cse.audible.sound.Audible;
import edu.msu.cse.audible.sound.AudibleChannel;
import edu.msu.cse.audible.sound.AudibleException;
import edu.msu.cse.audible.sound.SineVoice;

/**
 * This is a very simple AudibleSound example.
 * It creates the Audible system, creates a single output
 * channel, then attaches a sine-wave voice to the output.
 */
public class Sounder {
    /**
     * The Audible object in charge of sound
     */
    private Audible audible;
    
    /**
     * The main output channel object
     */
    private AudibleChannel channel = null;
    
    /**
     * The custom voice object
     */
    private CustomVoice voice = null;
    
    public Sounder() {
        /*
         * Connect to the Audible system
         */
        try {
            audible = Audible.createAudible();
        } catch (AudibleException ex) {
            System.err.println("Failed to set up AudibleSound - " + ex.getMessage());
            return;
        }
        
        /*
         * Create and open an audio channel
         * A audio channel is out connection to the system speakers.
         */
        channel = audible.createChannel();
        try {
            channel.open();
        } catch (AudibleException e) {
            System.err.println("Failed to open AudibleSound channel - " + e.getMessage());
        }
        
        /*
         * A voice is a sound generator. In this case
         * I am creating a custom sound generator
        */
        voice = new CustomVoice(audible);
        channel.addVoice(voice);
    }

    public CustomVoice getVoice() {
        return voice;
    }
         
    
    public void shutdown() {
        System.out.println("audio shutdown");
        if (channel != null) {
            try {
                channel.close();
            } catch (AudibleException e) {
            }
        }

    }
        
}
