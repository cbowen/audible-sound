package edu.msu.cse.audiblesound.example;

import edu.msu.cse.audible.sound.Audible;
import edu.msu.cse.audible.sound.Voice;

/**
 * This is the most basic possible custom voice.
 */
public class CircleVoice extends Voice {
    
    private float rotationFrequency = 0.5f;
    private float rotationPhase = 0;
    
    
    private float frequency = 440;
    
    private float amplitude = 1.0f;
    
    private double phase = 0;
    
    private float minFrequency = 200;
    private float maxFrequency = 500;
    
    public CircleVoice(Audible audible) {
        super(audible);
    }
    
    @Override
    synchronized public void update(float delta) {
        /*
         * Rotation computation
         */
        float rotationX = (float)Math.sin(rotationPhase);
        float rotationY = (float)Math.cos(rotationPhase);
        rotationPhase += delta * rotationFrequency * Math.PI * 2;

        /*
         * Amplitude left/right
         */
        float left = (rotationX + 1) / 2;
        float right = (-rotationX + 1) / 2;
        
        /*
         * Frequency up/down
        */
        float t = (rotationY + 1) / 2;
        frequency = minFrequency + t * (maxFrequency - minFrequency);
        
        /*
         * Generate the sample
        */
        float sample = amplitude * (float)Math.sin(phase);
        phase += delta * frequency * Math.PI * 2;
        audio[0] = left * sample;
        audio[1] = right * sample;
    }

    /**
     * @return the frequency
     */
    public float getFrequency() {
        return frequency;
    }

    /**
     * @param frequency the frequency to set
     */
    public void setFrequency(float frequency) {
        this.frequency = frequency;
    }

    /**
     * @return the amplitude
     */
    public float getAmplitude() {
        return amplitude;
    }

    /**
     * @param amplitude the amplitude to set
     */
    public void setAmplitude(float amplitude) {
        this.amplitude = amplitude;
    }

}
