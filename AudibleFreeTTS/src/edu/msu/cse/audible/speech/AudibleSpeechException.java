package edu.msu.cse.audible.speech;

/**
 * Exceptions in the Audible Speech system
 */
public class AudibleSpeechException extends Exception {

    /**
	 * Serialization ID
	 */
	private static final long serialVersionUID = -8697554652097884245L;

	public AudibleSpeechException(String msg) {
        super(msg);
    }
}
