package edu.msu.cse.audible.speech;

import com.sun.speech.freetts.audio.AudioPlayer;
import com.sun.speech.freetts.jsapi.FreeTTSEngineCentral;
import com.sun.speech.freetts.jsapi.FreeTTSVoice;
import com.sun.speech.freetts.lexicon.Lexicon;
import java.beans.PropertyVetoException;
import java.io.File;
import java.nio.ByteOrder;
import java.util.Locale;
import javax.sound.sampled.AudioFormat;
import javax.speech.AudioException;
import javax.speech.Central;
import javax.speech.EngineCreate;
import javax.speech.EngineException;
import javax.speech.EngineList;
import javax.speech.EngineStateError;
import javax.speech.synthesis.Synthesizer;
import javax.speech.synthesis.SynthesizerModeDesc;
import javax.speech.synthesis.SynthesizerProperties;
import javax.speech.synthesis.Voice;

/**
 * On object of this type should be instantiated in the system to communicate
 * with the speech system. This class converts speech into audio samples.
 */
public class AudibleSpeechFactoryBase {
    
    /** The voice synthesizer we will use */
    private Synthesizer synthesizer = null;
    
    /** The synthesizer voice */
    private Voice voice = null;
    
    /** The voice name we prefer to use */
    private String preferredVoiceName = "mbrola_us1";
            
    /** The voice name we want to use */
    private String voiceName = "";
    
    /** The desired voice mode */
    private String modeName = "general";
    
    private AudioFormat audioFormat = null;
    
    private boolean bigEndian = false;
    
    private float [] accumulator = null;
    
    private int accumulatorLoc = 0;
    
    private float sampleRate = 16000;
    
    /** The synthesizer properties. Sets things like speech rate, etc. */
    private SynthesizerProperties properties = null;
    
    public void AudibleSpeechFactory() {
        bigEndian = ByteOrder.nativeOrder().equals(ByteOrder.BIG_ENDIAN);
    }
    
    protected void openSpeech() throws AudibleSpeechException {
        try {
            // TODO: Automatically determine mborla.base
            System.setProperty("mbrola.base", "C:\\Program Files (x86)\\Mbrola Tools");
        } catch(Exception ex) {
        }
        
        /*
         * Find a voice that we can use and create a synthesizer
         */
        
        createSynthesizer();
        
        /* 
         * Get the synthesizer ready to speak
         */
        try {
            synthesizer.allocate(); 
            synthesizer.resume(); 
        } catch(EngineException ex) {
            throw new AudibleSpeechException("Engine exception, unable to allocate or resume speech synthesizer " + ex.getLocalizedMessage());
        } catch(AudioException ex) {
            throw new AudibleSpeechException("Audio exception, unable to allocate or resume speech synthesizer " + ex.getLocalizedMessage());
        }

        /* 
         * Choose the voice for this synthesizer
         */
        SynthesizerModeDesc desc = (SynthesizerModeDesc) synthesizer.getEngineModeDesc();
        Voice[] voices = desc.getVoices();
        voice = null;
        for (int i = 0; i < voices.length; i++) {
            if (voices[i].getName().equals(voiceName)) {
                voice = voices[i];
                break;
            }
        }

        if (voice == null) {
            throw new AudibleSpeechException("Synthesizer does not have a voice named " +
                    voiceName);
        }
        
        try {
            synthesizer.getSynthesizerProperties().setVoice(voice);
        } catch (PropertyVetoException ex) {
            
        }

        properties = synthesizer.getSynthesizerProperties();
        try {
            properties.setVolume(1);

        } catch (PropertyVetoException ex) {
        }
        
               
        if(voice instanceof FreeTTSVoice) {
            FreeTTSVoice fvoice = (FreeTTSVoice)voice;
            com.sun.speech.freetts.Voice ttsvoice = fvoice.getVoice();
            Lexicon lexicon = ttsvoice.getLexicon();
            addWords(lexicon);
        }
    }
    
    private void addWords(Lexicon lexicon) {
//        String [] p = lexicon.getPhones("controller", null);
//        int x = 0;
//        
//        String [] controller = {"k", "ao", "n", "t", "r", "ow1", "l", "er"};
//        lexicon.addAddendum("controller", null, controller);
        //String [] phonemes = {"aa1", "n", "aa"};
        //lexicon.addAddendum("on", null, phonemes);
    }
    
    /**
     * Add a word to the pronouncing dictionary.
     * @param word New work to add
     * @param phonemes List of phonemenes in CMU format
     */
    public void addWord(String word, String [] phonemes) {
        if(voice instanceof FreeTTSVoice) {
            FreeTTSVoice fvoice = (FreeTTSVoice)voice;
            com.sun.speech.freetts.Voice ttsvoice = fvoice.getVoice();
            Lexicon lexicon = ttsvoice.getLexicon();
            lexicon.addAddendum(word, null, phonemes);
        } 
    }
    
    public String [] getWord(String word) {
        if(voice instanceof FreeTTSVoice) {
            FreeTTSVoice fvoice = (FreeTTSVoice)voice;
            com.sun.speech.freetts.Voice ttsvoice = fvoice.getVoice();
            Lexicon lexicon = ttsvoice.getLexicon();
            return lexicon.getPhones(word, null);
        } 
        
        return null;
    }

    /**
     * Get the current volume.
     * @return Current volume
     */
    public float getVolume() {
        return properties.getVolume();
    }
    
    /**
     * Set the volume for the synthesizer's speech output as a value 
     * between 0.0 and 1.0. A value of 0.0 indicates silence. A value 
     * of 1.0 is maximum volume and is the synthesizer default.
     * 
     * A synthesizer may change the voice's style with volume. For 
     * example, a quiet volume might produce whispered output and 
     * loud might produce shouting. Most synthesizer do not 
     * make this type of change.
     * @param v 
     */
    public void setVolume(float v) {
        try {
            properties.setVolume(v);
        } catch (PropertyVetoException ex) {
        }
    }
    
    /**
     * Get the baseline pitch for synthesis.
     * @return Pitch
     */
    public float getPitch() {
        return properties.getPitch();
    }
    
    /**
     * Set the baseline pitch for the current synthesis voice. Out-of-range 
     * values may be ignored or restricted to engine-defined limits. Different 
     * voices have different natural sounding ranges of pitch. Typical 
     * male voices are between 80 and 180 Hertz. Female pitches typically 
     * vary from 150 to 300 Hertz.
     * @param p Pitch in Hertz
     */
    public void setPitch(float p) {
        try {
            properties.setPitch(p);
        } catch (PropertyVetoException ex) {
        }
    }
    
    /**
     * Get the pitch range for synthesis.
     * @return pitch range in Hertz
     */
    public float getPitchRange() {
        return properties.getPitchRange();
    }
    
    /**
     * Set the pitch range for the current synthesis voice. A narrow pitch 
     * range provides monotonous output while wide range provide a more 
     * lively voice. This setting is a hint to the synthesis engine. Engines 
     * may choose to ignore unreasonable requests. Some synthesizers may 
     * not support pitch variability.
     * 
     * The pitch range is typically between 20% and 80% of the baseline pitch.
     * @param r 
     */
    public void setPitchRange(float r) {
        try {
            properties.setPitchRange(r);
        } catch (PropertyVetoException ex) {
        }
    }
    
    /**
     * Get the current target speaking rate.
     * @return speaking rate in words per minute
     */
    public float getSpeakingRate() {
        return properties.getSpeakingRate();
    }
    
    /**
     * Set the target speaking rate for the synthesis voice in words per minute.
     * 
     * Reasonable speaking rates depend upon the synthesizer and the 
     * current voice (some voices sound better at higher or lower speed 
     * than others).
     * 
     * Speaking rate is also dependent upon the language because of different 
     * conventions for what is a "word". A reasonable speaking rate for 
     * English is 200 words per minute.
     * @param r Speaking rate in words per minute
     */
    public void setSpeakingRate(float r) {
        try {
            properties.setSpeakingRate(r);
        } catch (PropertyVetoException ex) {
        }
    }
    
    /**
     * Create the synthesizer, finding the best voice we can.
     * @throws Exception 
     */
    private void createSynthesizer() throws AudibleSpeechException {
        SynthesizerModeDesc desc = new SynthesizerModeDesc(
                null, // engine name
                "general", // mode name
                Locale.US, // locale
                Boolean.FALSE, // running
                null);         // voice
        
        /*
         * This code is designed to set up FreeTTS without looking for
         * that speech.properties file.
         */
        FreeTTSEngineCentral central;
        
        try {
            central = new FreeTTSEngineCentral();
        } catch (Exception ex) {
            throw new AudibleSpeechException("Unable to create FreeTTS engine");
        }
        
        EngineList engineList = central.createEngineList(desc);
        if(engineList == null) {
            throw new AudibleSpeechException("Unable locate any available speech engines");
        }
 
        // Retain the name of the first voice we find
        Voice fallbackVoice = null;
        int fallbackVoiceEngine = -1;
        
        // Retain the voice for the preferred voice if we find it
        Voice preferredVoice = null;
        int foundPreferredEngine = -1;
        
        for (int i = 0; i < engineList.size(); i++) {
            
            
            SynthesizerModeDesc engineDesc = (SynthesizerModeDesc) engineList.get(i);
//            System.out.println("    " + engineDesc.getEngineName()
//                               + " (mode=" + engineDesc.getModeName()
//                               + ", locale=" + engineDesc.getLocale() + "):");
            
            // Get the voices for this synthesizer
            Voice[] voices = engineDesc.getVoices();
            for (int j = 0; j < voices.length; j++) {
                String foundVoice = voices[j].getName();
                //System.out.println("        " + voices[j].getName());
                
                /*
                 * We keep track of any available voices, but we prefer the voice
                 * kevin16 over kevin if it is available.
                 */
                if(fallbackVoice == null || (fallbackVoice.getName().equals("keven") && foundVoice.equals("kevin16"))) {
                    fallbackVoice = voices[j];
                    fallbackVoiceEngine = i;
                }
                
                if(foundVoice.equals(preferredVoiceName)) {
                    preferredVoice = voices[j];
                    foundPreferredEngine = i;
                }
            }
        }
    
        EngineCreate creator;
        if(preferredVoice != null) {
            voiceName = preferredVoiceName;
            creator = (EngineCreate) engineList.get(foundPreferredEngine); 
        } else {
            if(fallbackVoice == null) {
                throw new AudibleSpeechException("No available speech voices");
            }
            
            voiceName = fallbackVoice.getName();
            creator = (EngineCreate) engineList.get(fallbackVoiceEngine); 
        } 
        
        try {
            synthesizer = (Synthesizer) creator.createEngine(); 
        } catch (IllegalArgumentException ex) {
            throw new AudibleSpeechException("Unable to create speech engine IllegalArgumentException " + ex.getMessage());
        } catch (EngineException ex) {
            throw new AudibleSpeechException("Unable to create speech engine EngineException " + ex.getMessage());
        } catch (SecurityException ex) {
            throw new AudibleSpeechException("Unable to create speech engine SecurityException " + ex.getMessage());
        }

        if (synthesizer == null) {
            throw new AudibleSpeechException("No voice synthesizer found");
        }  

    }
    
    protected void closeSpeech() throws AudibleSpeechException {
        if(synthesizer == null) {
            return;
        }
        
        synthesizer.pause();
        try {
            synthesizer.deallocate();
        } catch (EngineException ex) {
            throw new AudibleSpeechException("EnginException Unable to deallocate speech engine " + ex.getLocalizedMessage());
        } catch (EngineStateError ex) {
            throw new AudibleSpeechException("EngineStateError Unable to deallocate speech engine " + ex.getLocalizedMessage());
        }
        synthesizer = null;
        voice = null;
    }
    
    /**
     * Convert text into an array of float values containing the audio
     * spoken for the text.
     * @param text Text to speak
     * @return array of text.
     * @throws Exception 
     */
    public float [] speechToAudio(String text) throws Exception {
        
        if(!(voice instanceof com.sun.speech.freetts.jsapi.FreeTTSVoice)) {
            return null;
        }
        
        ((com.sun.speech.freetts.jsapi.FreeTTSVoice)voice).getVoice().setAudioPlayer(new AudioPlayer() {

            @Override
            public void setAudioFormat(AudioFormat af) {
                audioFormat = af;
                bigEndian = af.isBigEndian();
                sampleRate = af.getSampleRate();
            }

            @Override
            public AudioFormat getAudioFormat() {
                return audioFormat;
            }

            @Override
            public void pause() {
                
            }

            @Override
            public void resume() {
                
            }

            @Override
            public void reset() {
                
            }

            @Override
            public boolean drain() {
                return true;
            }

            @Override
            public void begin(int i) {
                accumulator = new float[i];
                accumulatorLoc = 0;
            }

            @Override
            public boolean end() {
                return true;
            }

            @Override
            public void cancel() {
                
            }

            @Override
            public void close() {
                
            }

            @Override
            public float getVolume() {
                return 1;
            }

            @Override
            public void setVolume(float f) {
                
            }

            @Override
            public long getTime() {
                return 0;
            }

            @Override
            public void resetTime() {
                
            }

            @Override
            public void startFirstSampleTimer() {
                
            }

            @Override
            public boolean write(byte[] bytes) {
                int len = bytes.length;
                if(bigEndian) {
                    for(int i=0;  i<len;  i+=2) {
                        int s = ((short)bytes[i] << 8) + ((short)bytes[i+1] & 0xff);
                        accumulator[accumulatorLoc++] = s * (1.0f / 32767.0f);
                    } 
                } else {
                    for(int i=0;  i<len;  i+=2) {
                        int s = ((short)bytes[i+1] << 8) + ((short)bytes[i] & 0xff);
                        accumulator[accumulatorLoc++] = s * (1.0f / 32768.0f);
                    }
                }
                
                return true;
            }

            @Override
            public boolean write(byte[] bytes, int i, int i1) {
                return true;
            }

            @Override
            public void showMetrics() {
                
            }
            
        });
        
        synthesizer.speak(text, null);
        synthesizer.waitEngineState(Synthesizer.QUEUE_EMPTY);
        
        return accumulator;
    }
    
    public void speakAudible(String text) {
        try {
            synthesizer.speak(text, null);
            synthesizer.waitEngineState(Synthesizer.QUEUE_EMPTY);
        } catch(Exception ex) {
        }
    }
    
    /**
     * This function looks at all available voices and sees if 
     * the one we want is there. If not, it finds whatever voice is 
     * available.
     */
    public void determineVoice() throws AudibleSpeechException {

        /* Create a template that tells JSAPI what kind of speech
         * synthesizer we are interested in.  In this case, we're
         * just looking for a general domain synthesizer for US
         * English.
         */ 
        SynthesizerModeDesc required = new SynthesizerModeDesc(
            null,      // engine name
            modeName,  // mode name
            Locale.US, // locale
            null,      // running
            null);     // voices

        // Retain the name of the first voice we find
        String firstVoice = null;
        
        // Set true if the voice we want is found
        boolean foundDesired = false;
        
        // Loop over all available synthesizers
        EngineList engineList = Central.availableSynthesizers(required);
        for (int i = 0; i < engineList.size(); i++) {
            
            SynthesizerModeDesc desc = (SynthesizerModeDesc) engineList.get(i);
            System.out.println("    " + desc.getEngineName()
                               + " (mode=" + desc.getModeName()
                               + ", locale=" + desc.getLocale() + "):");
            
            // Get the voices for this synthesizer
            Voice[] voices = desc.getVoices();
            for (int j = 0; j < voices.length; j++) {
                System.out.println("        " + voices[j].getName());
                if(firstVoice == null) {
                    firstVoice = voices[j].getName();
                }
                
                if(voices[j].getName().equals(voiceName)) {
                    foundDesired = true;
                }
            }
        }
        
        if(!foundDesired) {
            if(firstVoice == null) {
                throw new AudibleSpeechException("No available speech voices");
            }
            
            voiceName = firstVoice;
        }
    }
    
        /**
     * Returns a "no synthesizer" message, and asks 
     * the user to check if the "speech.properties" file is
     * at <code>user.home</code> or <code>java.home/lib</code>.
     *
     * @return a no synthesizer message
     */
    static private String noSynthesizerMessage() {
        String message =
            "No synthesizer created.  This may be the result of any\n" +
            "number of problems.  It's typically due to a missing\n" +
            "\"speech.properties\" file that should be at either of\n" +
            "these locations: \n\n";
        message += "user.home    : " + System.getProperty("user.home") + "\n";
        message += "java.home/lib: " + System.getProperty("java.home") +
	    File.separator + "lib\n\n" +
            "Another cause of this problem might be corrupt or missing\n" +
            "voice jar files in the freetts lib directory.  This problem\n" +
            "also sometimes arises when the freetts.jar file is corrupt\n" +
            "or missing.  Sorry about that.  Please check for these\n" +
            "various conditions and then try again.\n";
        return message;
    }

    /**
     * @return the sampleRate for the converted text
     */
    public float getSampleRate() {
        return sampleRate;
    }

    /**
     * Get the voice name for the voice we are actually using
     * @return the voiceName
     */
    public String getVoiceName() {
        return voiceName;
    }

    /**
     * @return the preferredVoiceName
     */
    public String getPreferredVoiceName() {
        return preferredVoiceName;
    }

    /**
     * @param preferredVoiceName the preferredVoiceName to set
     */
    public void setPreferredVoiceName(String preferredVoiceName) {
        this.preferredVoiceName = preferredVoiceName;
    }

    /**
     * @return the voice
     */
    public Voice getVoice() {
        return voice;
    }

}
