package edu.msu.cse.audible.speech;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Unit testing for the AudibleSpeechFactoryBase class.
 */
public class ShortTest {
    
    public ShortTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of AudibleSpeechFactory method, of class AudibleSpeechFactory.
     */
    @Test
    public void test() {
        System.out.println("AudibleSpeechFactory short test");
        AudibleSpeechFactoryBase factory = new AudibleSpeechFactoryBase();
        
        try {
            factory.setPreferredVoiceName("kevin16");
            factory.openSpeech();
            
            factory.setSpeakingRate(200);

            //float [] f = factory.speechToAudio("This is a test of using the Audible Speech Factory to generate audio");
            //System.out.println("Number of samples = " + f.length);
            String [] controller = {"k", "ao", "n", "t", "r", "ow1", "l", "er"};
            factory.addWord("controller", controller);
        
            factory.speakAudible("This is a test");
            factory.speakAudible("controller");
            factory.speakAudible("controller");
            factory.speakAudible("three");
            factory.speakAudible("four");

            factory.closeSpeech();
        } catch(Exception ex) {
            fail("Exeption: " + ex.getMessage());
        }
    }
   
}