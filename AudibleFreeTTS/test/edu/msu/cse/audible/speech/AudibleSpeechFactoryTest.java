package edu.msu.cse.audible.speech;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Unit testing for the AudibleSpeechFactoryBase class.
 */
public class AudibleSpeechFactoryTest {
    
    public AudibleSpeechFactoryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of AudibleSpeechFactory method, of class AudibleSpeechFactory.
     */
    @Test
    public void test() {
        System.out.println("AudibleSpeechFactory test");
        AudibleSpeechFactoryBase factory = new AudibleSpeechFactoryBase();
        
        try {
            factory.openSpeech();

            //float [] f = factory.speechToAudio("This is a test of using the Audible Speech Factory to generate audio");
            //System.out.println("Number of samples = " + f.length);

            factory.speakAudible("This is an em brola voice");
            factory.speakAudible("on");
            factory.speakAudible("three");
            factory.speakAudible("four");

            factory.closeSpeech();
        } catch(Exception ex) {
            fail("Exeption: " + ex.getMessage());
        }
    }
    
    @Test
    public void test2() {
        System.out.println("AudibleSpeechFactory test voice fallback");
        AudibleSpeechFactoryBase factory = new AudibleSpeechFactoryBase();
        
        try {
            factory.setPreferredVoiceName("xxx");
            factory.openSpeech();

            //float [] f = factory.speechToAudio("This is a test of using the Audible Speech Factory to generate audio");
            //System.out.println("Number of samples = " + f.length);

            factory.speakAudible("This should fall back to an available voice");

            factory.closeSpeech();
        } catch(Exception ex) {
            fail("Exeption: " + ex.getMessage());
        }  
    }
    
    /**
     * Test that we can create and use two factories at the same time.
     */
    @Test
    public void test3() {
        System.out.println("AudibleSpeechFactory test 2 factories");
        AudibleSpeechFactoryBase factory1 = new AudibleSpeechFactoryBase();
        
        try {
            factory1.setPreferredVoiceName("mbrola_us1");
            factory1.openSpeech();
        } catch(Exception ex) {
            fail("Exeption: " + ex.getMessage());
        } 
        
        AudibleSpeechFactoryBase factory2 = new AudibleSpeechFactoryBase();
        
        try {
            factory2.setPreferredVoiceName("kevin16");
            factory2.openSpeech();
        } catch(Exception ex) {
            fail("Exeption: " + ex.getMessage());
        } 
        
        try {
            factory1.speakAudible("Two factories test: on em brola u s one");
            factory2.speakAudible("Two factories test: kevin 16");
            
            factory1.closeSpeech();
            factory2.closeSpeech();
        } catch(Exception ex) {
            fail("Exeption: " + ex.getMessage());
        } 
    }

}