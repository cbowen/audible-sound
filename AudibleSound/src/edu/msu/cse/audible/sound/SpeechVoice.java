package edu.msu.cse.audible.sound;

/**
 * A voice generated from speech.
 */
public class SpeechVoice extends Voice {
    
    private float [] samples;
    private float sampleRate;
    private float position = 0;
    
    private int end = 0;
    
    /**
     * The channel gain left/right
     */
    private float [] gain = {1, 1};
    
    public SpeechVoice(Audible audible, float [] samples, int start, int end, float sampleRate) {
        super(audible);
        
        this.samples = samples;
        this.end = end;
        this.sampleRate = sampleRate;
        this.position = start;
    }

    @Override
    public void update(float delta) {
        position += delta * sampleRate;
        float sample;
        
        if(position < end - 1) {
            int p = (int)position;
            float f = position - p;
            
            sample = samples[p] * (1-f) + samples[p] * f;
        } else {
            sample = 0;
        }
        
        audio[0] = sample * gain[0];
        audio[1] = sample * gain[1];
    }
    
    @Override
    public boolean isDone() {
        return position >= end;
    }
    
    /**
     * Set the gain for a given channel.
     * @param channel Channel index 0=left, 1=right
     * @param gain Gain to set [0-1]
     */
    public void setGain(int channel, float gain) {
    	this.gain[channel] = gain;
    }
    
    /**
     * Set the stereo gain of the two channels.
     * @param left
     * @param right
     */
    public void setGains(float left, float right) {
    	this.gain[0] = left;
    	this.gain[1] = right;
    }
}
