package edu.msu.cse.audible.sound;

import java.net.URL;

/**
 * Implements a playback of a wave table that can load audio from a resource
 * or create it or provide a place to put it. The audio can then
 * be played. See the {@link WaveTable} class for the actual audio storage.
 */
public class WaveTableVoice extends Voice implements WaveTable.User {
    /**
     * Playback sample rate
     */
    private float sampleRate = 0;
    

     
    /** Are we looping this audio? */
    private volatile boolean looping = false;
    
    /*
     * The actual playback rate is playFrequency / nativeFrequency. So
     * it does not really matter what these values are, only the ratio 
     * matters.
     */
    
    /** Playback frequency */
    private volatile float playFrequency = 1;
    
    /** The result of playFrequency / nativeFrequency */
    private volatile float multiplier = 1;
    
    /** 
     * Location in the audio 
     */
    private volatile double location = 0;
    
    /**
     * The wavetable to use
     */
    private WaveTable wavetable = null;
    
    /*
     * The following information is copied from 
     * the currently installed wavetable.
     */
   
    /** 
     * The actual audio data 
     */
    private float [][] wave = null;
    
    /** 
     * The number of channels 
     */
    private int channels = 1;
    
    /** 
     * How many audio frames are there? 
     */
    private int frames = 0;
    
    
    /**
     * Constructor
     * @param audible Audible system to use
     */
    public WaveTableVoice(Audible audible) {
        super(audible);
        
        sampleRate = audible.getSampleRate();
    }
    
    /**
     * Constructor. This version accepts a wave table to
     * use.
     * @param audible Audible system to sue
     * @param wavetable WaveTable object to use
     */
    public WaveTableVoice(Audible audible, WaveTable wavetable) {
        this(audible);
        
        setWavetable(wavetable);
    }


    /**
     * Update the object in time.
     * @param delta Elapsed time (usually sample period)
     */
    @Override
    public void update(float delta) {
        if(wavetable == null) {
            audio[0] = 0;
            audio[1] = 0;
            return;
        }
      
        if(location < frames - 1) {
            int frame = (int)location;  // Integer frame
            float f = (float)(location - frame); // Fractional part
            
            if(channels == 2) {
                audio[0] = wave[0][frame] * (1-f) + wave[0][frame+1] * f;
                audio[1] = wave[1][frame] * (1-f) + wave[1][frame+1] * f;
            } else {
                float sample = wave[0][frame] * (1-f) + wave[0][frame+1] * f;
                audio[0] = sample;
                audio[1] = sample;
            }
        } else if(looping) {
            // We must be at or after the last frame. If we are looping, we so something 
            // special here
            int frame1 = (int)location;  // Integer frame
            float f = (float)(location - frame1); // Fractional part
            frame1 %= frames;
            int frame2 = (frame1 + 1) % frames;
            
            if(channels == 2) {
                audio[0] = wave[0][frame1] * (1-f) + wave[0][frame2] * f;
                audio[1] = wave[1][frame1] * (1-f) + wave[1][frame2] * f;
            } else {
                float sample = wave[0][frame1] * (1-f) + wave[0][frame2] * f;
                audio[0] = sample;
                audio[1] = sample;
            }

            while(location > frames) {
                location -= frames;
            }
        } else {
            audio[0] = 0;
            audio[1] = 0;
        }
        
        // And advance the location
        // This may go past the end, in which case, it will
        // be caught the next time update is called.
        location += delta * sampleRate * multiplier; 
    }
    
    /**
     * Reset the voice to be beginning of the wave table
     */
    public void reset() {
    	location = 0;
    }
    
    /**
     * Is this voice done?
     * @return true if the voice is done
     */
    @Override
    public boolean isDone() {
        return !looping && location >= (frames - 1);
    }
    

    /**
     * Load an audio file from a Java resource (URL).
     * Automatically allocates a wavetable to use.
     * @param url resource identification
     * @throws AudibleException If file type is not supported or does not exist or cannot be read.
     */
    public void loadFromResource(URL url) throws AudibleException {
        wavetable = new WaveTable(audible);
        wavetable.loadFromResource(url);
        wavetable.addUser(this);
        getWavetableInfo();
    }
    
    /**
     * Load an audio file.
     * Automatically allocates a wavetable to use.
     * @param filename Filename to load
     * @throws AudibleException If file type is not supported or does not exist or cannot be read.
     */
    public void loadFromFile(String filename) throws AudibleException {
        wavetable = new WaveTable(audible);
        wavetable.loadFromFile(filename);
        wavetable.addUser(this);
        getWavetableInfo();
    }
    
    /**
     * Get the playback frequency for this object. The 
     * playback frequency is used in conjuction with the native
     * frequency of the wavetable to determine the playback
     * rate multiplier.
     * @return The playback frequency selected for this object.
     */
    public float getPlayFrequency() {
        return playFrequency;
    }

    /**
     * Set the playback frequency for this object. The 
     * playback frequency is used in conjuction with the native
     * frequency of the wavetable to determine the playback
     * rate multiplier.
     * @param playFrequency the playFrequency to set
     */
    public void setPlayFrequency(float playFrequency) {
        this.playFrequency = playFrequency;
        this.multiplier  = playFrequency / wavetable.getNativeFrequency();
    }



    /**
     * Get the looping status of the voice. If looping is set, the 
     * playback will loop back to the beginning when it reached the
     * end.
     * @return looping flag
     */
    public boolean isLooping() {
        return looping;
    }

    /**
     * Set the looping status of the voice. If looping is set, the 
     * playback will loop back to the beginning when it reached the
     * end.
     * @param looping looping flag setting
     */
    public void setLooping(boolean looping) {
        this.looping = looping;
    }

    /**
     * Get the installed wavetable this is playing
     * @return the wavetable object or null if none
     */
    public final WaveTable getWavetable() {
        return wavetable;
    }

    /**
     * Set the installed wavetable this is playing
     * @param wavetable wavetable reference
     */
    public final void setWavetable(WaveTable wavetable) {
        if(this.wavetable != null) {
            this.wavetable.removeUser(this);
        }
        
        this.wavetable = wavetable;
        if(this.wavetable != null) {
            this.wavetable.addUser(this);
        }
        
        getWavetableInfo();
    }

    /**
     * This function is called by the WaveTable whenever
     * the parameters of the table are changed.
     */
    @Override
    public void waveTableChanged() {
        getWavetableInfo();
    }
    
    /**
     * To make this faster, we copy information from
     * the wavetable to local member variables. This gets
     * that information.
     */
    void getWavetableInfo() {
        if(wavetable != null) {
            wave = wavetable.getWave();
            frames = wavetable.getFrames();
            channels = wavetable.getChannels();
            multiplier = playFrequency / wavetable.getNativeFrequency();
        } else {
            // Default values to use if no wavetable available.
            wave = null;
            frames = 0;
            channels = 1;
            multiplier = 1;
        }
        
    }

}
