package edu.msu.cse.audible.sound;

/**
 * A simple sine wave voice. Mostly used as a test case.
 */
public class SineVoice extends Voice {
    
    private float frequency = 440;
    
    private float amplitude = 1.0f;
    
    private double phase = 0;
    
    public SineVoice(Audible audible) {
        super(audible);
    }
    
    @Override
    synchronized public void update(float delta) {
        float sample = amplitude * (float)Math.sin(phase);
        phase += delta * frequency * Math.PI * 2;
        audio[0] = sample;
        audio[1] = sample;
    }

    /**
     * @return the frequency
     */
    public float getFrequency() {
        return frequency;
    }

    /**
     * @param frequency the frequency to set
     */
    public void setFrequency(float frequency) {
        this.frequency = frequency;
    }
    
    /**
     * Set the frequency using a MIDI note name
     * @param note Name of note. Notice: Case sensitive, upper case 
     * notes only.
     */
    public void setNote(String note) {
        this.frequency = Notes.noteToFreq(note);
    }

    /**
     * @return the amplitude
     */
    public float getAmplitude() {
        return amplitude;
    }

    /**
     * @param amplitude the amplitude to set
     */
    public void setAmplitude(float amplitude) {
        this.amplitude = amplitude;
    }
}
