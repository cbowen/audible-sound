/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.msu.cse.audible.sound;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Mixer;

/**
 *
 * @author cbowen
 */
public class AudibleSys extends Audible {
    
    /** Sample rate in samples per second */
    private float sampleRate = 44100;
    
    /** Sample period in seconds */
    private float samplePeriod = 1.0f / 44100.0f;
    
    /**
     * A collection of all known audio channels.
     */
    private final List<AudibleChannel> channels = new LinkedList<AudibleChannel>();
    
    /**
     * A collection of all mixers we know about. This 
     * allows us to only call AudioSystem.getMixer() one time
     * for any given mixer, since it may create new copies.
     */
    private final TreeMap<String, Mixer> mixers = new TreeMap<String, Mixer>();
    
    /**
     * Constructor. Creates the audible system.
     * @throws AudibleException 
     */
    protected AudibleSys() throws AudibleException {
    }

    /**
     * Create an audio channel using the default mixer.
     * @return Audible channel
     */
    @Override
    public AudibleChannel createChannel() {
        AudibleChannel channel = new AudibleChannel(this);
        addChannel(channel);
        return channel;
    }
    
    /**
     * Get the default mixer.
     * @return Default Java sound mixer
     * @throws AudibleException If unable to obtain the default mixer
     */
    public Mixer getMixer() throws AudibleException {
        // Does the default mixer already exist?
        // If so, we can just return it.
        Mixer mixer = mixers.get("");
        if(mixer == null) {
            // If not, we have to create it
            try {
                mixer = AudioSystem.getMixer(null);
            } catch (Exception ex) { 
                throw new AudibleException("Unable to obtain audio mixer");
            }
            
            // Add to the collection
            mixers.put("", mixer);
        }
        return mixer;
    }
    
    /**
     * Get a named mixer.
     * @param name The mixer name to use.
     * @return Default Java sound mixer
     * @throws AudibleException If unable to obtain the default mixer
     */
    public Mixer getMixer(String name) throws AudibleException {
        // Does the default mixer already exist?
        // If so, we can just return it.
        Mixer mixer = mixers.get(name);
        if(mixer == null) {
            if(name.equals("")) {
                return getMixer();
            }
            
            Mixer.Info[] infos = AudioSystem.getMixerInfo();
            Mixer.Info info = null;
            for(Mixer.Info infoTest : infos) {
                if(infoTest.getName().equals(name)) {
                    info = infoTest;
                    break;
                }
            }
            
            if(info == null) {
                // Did not find the channel
                throw new AudibleException("Audio mixer " + name + " does not exist");
            }
            
            // If not, we have to create it
            try {
                mixer = AudioSystem.getMixer(info);
            } catch (Exception ex) { 
                throw new AudibleException("Unable to obtain audio mixer " + name);
            }
            
            // Add to the collection
            mixers.put(name, mixer);
        }
        return mixer;
    }
    
    
    @Override
    public List<String> getMixerNames() {
        Mixer.Info[] infos = AudioSystem.getMixerInfo();
        ArrayList<String> names = new ArrayList<String>();
        for(Mixer.Info info : infos) {
            names.add(info.getName());
        }
        return names;
    }
    
    public void addChannel(AudibleChannel channel) {
        channels.add(channel);
    }
    
    public void removeChannel(AudibleChannel channel) {
        channels.remove(channel);
    }
    
    @Override
    public boolean hasChannel(AudibleChannel channel) {
        return channels.contains(channel);
    }

    /**
     * Get the sample rate
     * @return the sampleRate
     */
    @Override
    public float getSampleRate() {
        return sampleRate;
    }
    
    /**
     * Get the sample period in seconds
     * @return the sample period
     */
    @Override
    public float getSamplePeriod() {
        return samplePeriod;
    }

    /**
     * Set the sample rate. Must be done before any channels are
     * created. We only allow one sample rate in the system. 
     * 
     * The default sample rate is 44,100 samples per second
     * 
     * @param sampleRate the sampleRate to set
     */
    @Override
    public void setSampleRate(float sampleRate) {
        this.sampleRate = sampleRate;
        this.samplePeriod = 1.0f / sampleRate;
    }
    
}
