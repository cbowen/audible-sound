/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.msu.cse.audible.sound;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.LinkedList;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.SourceDataLine;

/**
 * This is a channel that can generate audio in the system.
 */
public class AudibleChannel extends Thread {

    /** The system this audible is a part of */
    private AudibleSys audible;
    
    /** The audio line we are connecting to */
    private SourceDataLine line = null;
    
    /** Number of audio channel */
    private int numChannels = 2;
    
    /** Sample size in bits */
    private final int sampleSize = 16;
    
    /** Requested buffer size in sample frames */
    private int bufferSize = 3000;
    
    /** Computed frame size in bytes */
    private int frameSize;
   
    private boolean bigEndian;
    
    private volatile boolean running = false;
    
    /** The list of voices current active for this object */
    private LinkedList<Voice> voices = new LinkedList<Voice>();
    
    /** A list of voices that are no longer active and can be deleted */
    private ArrayList<Voice> voicesToDelete = new ArrayList<Voice>();
    
    public AudibleChannel(AudibleSys audible) {
        this.audible = audible;
    }
    
    public Audible getAudible() {
        return audible;
    }

    /**
     * 
     * @return the sampleRate
     */
    public float getSampleRate() {
        return audible.getSampleRate();
    }

    /**
     * @return the numChannels
     */
    public int getNumChannels() {
        return numChannels;
    }

    /**
     * Set the number of channels. Must be done before the channel is opened.
     * @param numChannels the numChannels to set
     */
    public void setNumChannels(int numChannels) {
        this.numChannels = numChannels;
    }
    
    /**
     * Open the audio channel on the default mixer.
     * @throws AudibleException 
     */
    public void open() throws AudibleException {
        open("");
    }
    
    /**
     * Open an audio channel for output
     * @param name Mixer name
     * @throws AudibleException 
     */
    public void open(String name) throws AudibleException {
        //
        // Create an audio format item
        //
        frameSize = sampleSize * numChannels / 8;
        bigEndian = ByteOrder.nativeOrder().equals(ByteOrder.BIG_ENDIAN);
        float sampleRate = audible.getSampleRate();
        
        AudioFormat format = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED,
                sampleRate, 
                sampleSize, 
                numChannels, 
                frameSize, 
                frameSize * sampleRate, 
                bigEndian);
        
        Mixer mixer = audible.getMixer(name);
            
        //
        // Get the data line from the mixer
        //
        
        DataLine.Info info = new DataLine.Info(SourceDataLine.class, format);
        if(!mixer.isLineSupported(info)) {
            throw new AudibleException("Line type is not supported");
        }
        
        try {
            line = (SourceDataLine)mixer.getLine(info);
        } catch (LineUnavailableException ex) {
            line = null;
            throw new AudibleException("Unable to obtain a mixer line of the correct type");
        }
        
        //
        // Open and start the stream
        //
        
        try {
            line.open(format, bufferSize * frameSize);
            line.start();
        } catch (LineUnavailableException ex) {
            line = null;
            throw new AudibleException("Unable to open audio line");
        }     
                
        //
        // Start the thread
        //
        start();
    }
    
    /**
     * This is the thread that generates the audio. It allocates a 
     * buffer that we put audio into. 
     */
    public void run() {
        running = true;
        
        // This is how much buffer space we allocate
        // I make this half the buffer size we use for the 
        // output so we will not block for too long
        ByteBuffer buffer = ByteBuffer.allocate(bufferSize / 2 * frameSize);
        buffer.order(bigEndian ? ByteOrder.BIG_ENDIAN : ByteOrder.LITTLE_ENDIAN);
        
        // Get the buffer as a short buffer
        ShortBuffer sbuffer = buffer.asShortBuffer();
        int frame = 0;
        
        while(running) {
            sbuffer.position(0);    // First location in buffer
            
            // Fill the buffer
            for(int j=0;  j<bufferSize / 2;  j++) {
                //
                // Get a frame from each voice and 
                // add to a total for each channel
                //
                
                float c0 = 0;   // Left channel
                float c1 = 0;   // Right channel
                
                synchronized(this) {
                    for(Voice voice : voices) {
                        // Hold a lock on each object we are are accessing
                        synchronized(voice) {
                            // For each voice, either get audio from it
                            // or, if done, save for later deletion
                            if(voice.isDone()) {
                                voicesToDelete.add(voice);
                            } else {
                                voice.frame(frame);
                                float [] audio = voice.get();
                                c0 += audio[0];
                                c1 += audio[1];
                            }
                        }
                    }

                    //
                    // Delete any voices that are done
                    //
                    if(!voicesToDelete.isEmpty()) {
                        for(Voice voice : voicesToDelete) {
                            voices.remove(voice);
                        }

                        voicesToDelete.clear();
                    }
                }
                
                
                //
                // Range bounding and put into the buffer
                //
                
                if(c0 < -1) {
                    c0 = -1;
                } else if(c0 > 1) {
                    c0 = 1;
                }
                
                if(c1 < -1) {
                    c1 = -1;
                } else if(c1 > 1) {
                    c1 = 1;
                }
                
                sbuffer.put((short)(c0 * 32767));
                sbuffer.put((short)(c1 * 32767));
                

                
//                float t = (float)i / (float)44100;
//                short s = (short)(10000 * Math.sin(t * Math.PI * 2 * 440));
//                sbuffer.put(s);
//                sbuffer.put(s);
                
                frame++;
            }
            
            // Write the buffer to the line
            line.write(buffer.array(), 0, buffer.capacity());  
        }
    }
    
    public synchronized void close() throws AudibleException {
        // Stop the thread
        running = false;
        
        try {
            // Wait for thread to end
            join();
        } catch (InterruptedException ex) {
        }
        
        if(line == null) {
            throw new AudibleException("Attempting to close channel that is not open");
        }
        
        line.close();
        line = null;
    }
    
    public synchronized void addVoice(Voice voice) {
        voices.add(voice);
    }
    
    public synchronized void removeVoice(Voice voice) {
        voices.remove(voice);
    }
    
    public synchronized boolean hasVoice(Voice voice) {
        return voices.contains(voice);
    }
}
