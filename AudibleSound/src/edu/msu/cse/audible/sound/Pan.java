package edu.msu.cse.audible.sound;

/**
 * This audio filter creates a simple panning filter that allows for
 * linear changes in left-right placement of a sound. The 
 * placement can be sequenced.
 */
public class Pan extends Filter {
    
    /** The interpolation sequencer than runs the amplitude changes */
    private InterpolateSequence sequencer = new InterpolateSequence();
    
    /**
     * Constructor
     * @param audible Audible object we are a member of
     */
    public Pan(Audible audible) {
        super(audible); 
    }
        
    /**
     * Constructor
     * @param audible Audible object we are a member of
     * @param source A source that is automatically installed.
     */
    public Pan(Audible audible, Voice source) {
        super(audible, source);
    }
    
    /**
     * Update the filter in time.
     * @param delta Amount of time since last call to update
     */
    @Override
    public void update(float delta) {
        sequencer.update(delta);
        
        if(source != null) {
           float [] sourceAudio = source.get();
           float pan = sequencer.getValue();
           float lgain = (-pan + 1);
           float rgain = (pan + 1);
           
           audio[0] = sourceAudio[0] * lgain;
           audio[1] = sourceAudio[1] * rgain;
        } else {
            audio[0] = audio[1] = 0;
        }
    }
    
    /**
     * Set an immediate amount of panning.
     * @param p Pan to set. -1 = left, 1 = right.
     */
    public void set(float p) {
        sequencer.setValue(p);
    }
    
    /**
     * Is this sequence done? A done condition is indicated by
     * hitting an end event in the sequence.
     * @return true if an end event has been hit
     */
    public boolean isDone() {
        return sequencer.isDone() || (source != null && source.isDone());
    }

    /**
     * Create a linear interpolation sequence.
     * @param target Target amplitude, normally 0 to 1
     * @param duration Amount of time to reach target
     */
    void linear(float target, float duration) {
        sequencer.linear(target, duration);
    }
    
    /**
     * Create a delay event in the sequence.
     * @param duration Amount of delay
     */
    void delay(float duration) {
        sequencer.delay(duration);
    }
    
    /**
     * Create an end event in the sequence.
     */
    void end() {
        sequencer.end();
    }
    
}
