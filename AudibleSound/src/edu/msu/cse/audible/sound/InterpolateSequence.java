package edu.msu.cse.audible.sound;

import java.util.LinkedList;

/**
 * An interpolation sequence is a sequence of
 * events that occur that can be set in time and
 * will cause values to interpolate to other values.
 */
public class InterpolateSequence {
    
    /** The current interpolation time */
    //private float time = 0;
    
    /** The interpolated current value */
    private float value = 0;
    
    /** Keeps track of when we are done */
    private boolean done = false;
    
    /** An event in the list of events */
    private class Event {
        public float duration = 0;
        
        public Event(float duration) {
            this.duration = duration;
        }
        
        public void update(float delta) {
            duration -= delta;
        }
        
        public void end() {
        }
        
        public void start() {
            
        }
    }
    
    private class LinearEvent extends Event {
        public float target = 0;
        public float rate = 0;
        
        public LinearEvent(float target, float duration) {
            super(duration);
            this.target = target;
        }
        
        @Override
        public void update(float delta) {
            super.update(delta);
            value += delta * rate;
        }
        
        public void start() {
            rate = (target - value) / duration;
        }
        
        @Override
        public void end() {
            value = target;
        }
    }
    
    private class EndEvent extends Event {
        
        public EndEvent(float duration) {
            super(duration);
        }
                
        @Override
        public void end() {
            done = true;
        }
    }
    
    /** The list of pending events */
    private LinkedList<Event> pending = new LinkedList<Event>();
    
    /** Any currently active event */
    private Event active = null;
    
    public InterpolateSequence() {
  
    }
    
    /** Clear all pending events and any active event. */
    public void clear() {
        pending.clear();
        active = null;
    }
    
    /**
     * Get the current computed value
     * @return The value
     */
    public float getValue() {
        return value;
    }
    
    /**
     * Set the value. Note that this value may be
     * overwritten by sequencing.
     * @param value Value to set
     */
    public void setValue(float value) {
        this.value = value;
    }
    
    /**
     * Has the end event been reached?
     * @return True if end has executed.
     */
    public boolean isDone() {
        return done;
    }
    
    /**
     * Completely reset the object to zero.
     */
    public void reset() {
        clear();
        value = 0;
        done = false;
    }
    
    /**
     * Advance the time.
     * @param delta Amount to advance the time
     */
    public void update(float delta) {
        // Loop that consumes time and
        // processes event.
        while(active != null) {
            // If the remaining time is less than 
            // the delta value, only consume the 
            // remaining time and call end on the event.
            if(active.duration <= delta) {
                delta -= active.duration;

                active.end();
                active = null;

                pendingToActive();
            } else {
                // Otherwise, update the event by the delta
                // amount of time and we are done.
                active.update(delta);
                return;
            }

        }
    }
    
    /**
     * Create a linear interpolation event.
     * @param target Target value
     * @param duration Time to reach the target value
     */
    public void linear(float target, float duration) {
        pending.add(new LinearEvent(target, duration));
        pendingToActive();
    }
    
    /**
     * Create a delay event
     * @param duration Time to delay
     */
    public void delay(float duration) {
        pending.add(new Event(duration));
        pendingToActive();
    }
    
    /**
     * Create an end event. This event sets the
     * done flag when it occurs.
     */
    public void end() {
        pending.add(new EndEvent(0));
        pendingToActive();
    }
    
    /**
     * Get the number of pending events, not including
     * any active event.
     * @return Number of pending events
     */
    public int numPending() {
        return pending.size();
    }
    
    /**
     * Get if there is an active event
     * @return true if an event is active
     */
    public boolean isActive() {
        return active != null;
    }
    
    /**
     * If no event is active and the pending list is 
     * empty, move an item from pending to active.
     */
    private void pendingToActive() {
        if(active == null && !pending.isEmpty()) {
            active = pending.removeFirst();
            active.start();
            done = false;
        }
    }

}
