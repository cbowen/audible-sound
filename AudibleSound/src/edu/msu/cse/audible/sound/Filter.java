package edu.msu.cse.audible.sound;

/**
 * A Filter is a voice with a single source
 * that it can read from, process, then output.
 */
public abstract class Filter extends Voice {
    
    protected Voice source = null;
    
    public Filter(Audible audible) {
        super(audible);
    }
    
    public Filter(Audible audible, Voice source) {
        super(audible);
        
        this.source = source;
    }
    
    @Override
    public void frame(int f) {
        // Ensure the child has the right frame
        if(source != null) {
            source.frame(f);
        }
        
        super.frame(f);
    }

    /**
     * @return the source
     */
    public Voice getSource() {
        return source;
    }

    /**
     * @param source the source to set
     */
    public void setSource(Voice source) {
        this.source = source;
    }

}
