package edu.msu.cse.audible.sound;

import java.util.List;

/**
 * This is the main class for managing sound production in the
 * audible series. Updated.
 * @author Charles B. Owen
 */
public abstract class Audible {
    
    /**
     * Get a pointer to an Audible object. This is
     * the first call for using the system.
     * @return An Audible system object.
     */
    public static Audible createAudible() throws AudibleException {
        // Create and return the object
        return new AudibleSys();
    }
    
    /**
     * Constructor
     * @throws AudibleException 
     */
    protected Audible() throws AudibleException {
        

    }
    
    /**
     * Get a list of available mixer names as strings.
     * @return List of mixer names.
     */
    public abstract List<String> getMixerNames();
    
    /**
     * Create a channel. Channels are a threaded sound generator
     * connected to a mixer.
     * @return Channel
     */
    public abstract AudibleChannel createChannel();
    
    /**
     * Test that a channel exists in the system.
     * @param channel Channel object
     * @return true if channel exists.
     */
    public abstract boolean hasChannel(AudibleChannel channel);
    public abstract float getSampleRate();
    public abstract void setSampleRate(float sampleRate);
    public abstract float getSamplePeriod();
}
