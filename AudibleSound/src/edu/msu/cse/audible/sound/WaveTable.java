package edu.msu.cse.audible.sound;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * The wavetable class represents audio data in a wave
 * table that can be used for playback, usually by {@link WaveTableVoice}
 */
public class WaveTable {

    public interface User {
        void waveTableChanged();
    }
    
    /**
     * Every Audible component is associated with an Audible
     * object that maintains information such as the sample rate.
     */
    protected Audible audible;
    
    /** 
     * The actual audio data 
     */
    private float [][] wave = null;
    
    /** 
     * The number of channels 
     */
    private int channels = 1;
    
    /** 
     * How many audio frames are there? 
     */
    private int frames = 0;
    
    /** 
     * The native frequency of the wave 
     */
    private float nativeFrequency = 1;
    
    /**
     * This maintains an association to all users that
     * are using this wave table. This allows them to be
     * updated if the wave table is updated.
     */
    private final ArrayList<User> users = new ArrayList<User>();
    
    /**
     * Constructor
     * @param audible Audible system we are a member of
     */
    public WaveTable(Audible audible) {
        this.audible = audible;
    }
    
    /**
     * Allocate an audio buffer. The number of channels must be set 
     * before this function is called.
     * @param size 
     */
    public void allocate(int size) {
        frames = size;
        wave = new float[channels][size];
        for(int c=0;  c<channels;  c++) {
            for(int i=0;  i<size;  i++) {
                wave[c][i] = 0;
            }
        }
        
        notifyUsers();
    }
    
    
    /**
     * Load an audio file from a Java resource (URL).
     * @param url resource identification
     * @throws AudibleException If file type is not supported or does not exist or cannot be read.
     */
    public void loadFromResource(URL url) throws AudibleException {

        AudioInputStream stream;
        try {
            stream = AudioSystem.getAudioInputStream(url);
        } catch (UnsupportedAudioFileException ex) {
            throw new AudibleException("Audio resource file type not supported for " + url.getFile());
        } catch (IOException ex) {
            throw new AudibleException("I/O Exception reading audio resource " + url.getFile());
        }
        
        load(stream, url.getFile());
    }
    
    /**
     * Load an audio file
     * @param filename Filename to load
     * @throws AudibleException If file type is not supported or does not exist or cannot be read.
     */
    public void loadFromFile(String filename) throws AudibleException {
        AudioInputStream stream;
        
        try {
            stream = AudioSystem.getAudioInputStream(new File(filename));
        } catch (UnsupportedAudioFileException ex) {
            throw new AudibleException("Audio file type not supported for " + filename);
        } catch (IOException ex) {
            throw new AudibleException("I/O Exception reading audio file " + filename);
        }
        
        load(stream, filename);
    }
    
    
    /**
     * Actual load of audio data from the audio input stream.
     * @param stream Input stream to load from
     * @param source Name of the source (for exceptions)
     * @throws AudibleException If fails to load.
     */
    private void load(AudioInputStream stream, String source) throws AudibleException {
      
        try {
            AudioFormat format = stream.getFormat();
            if(format.getSampleSizeInBits() != 16) {
                throw new AudibleException("Audio resource file type not supported for " + source);
            }
            int numChannels = format.getChannels();
            int numFrames = (int)stream.getFrameLength();
            //int size = (int)(numFrames * format.getFrameSize());
            
            // Allocate!
            channels = numChannels;
            frames = numFrames;
            wave = new float[channels][frames];
            int waveLoading = 0;        // How much has been loaded so far?
            
            // Allocate a buffer to load the audio into 
            // We load in blocks of 8K at a time. 
            byte [] buffer = new byte[8192];
            
            // Read the data from the file a block 
            // at a time.
            int bytesRead;
            while((bytesRead = stream.read(buffer)) > 0 && waveLoading < frames) {
            
                // How many frames did we read?
                int framesRead = bytesRead / (numChannels * 2);

                // Copy the data over
                if(format.isBigEndian()) {
                    int ndx = 0;
                    for(int i=0;  i<framesRead && waveLoading < frames;  i++) {
                        for(int c=0;  c<channels; c++) {
                            int s = ((short)buffer[ndx] << 8) + ((short)buffer[ndx+1] & 0xff);
                            ndx += 2;
                            wave[c][waveLoading] = s * (1.0f / 32767.0f);
                        }
                        
                        waveLoading++;
                    } 
                } else {
                    int ndx = 0;
                    for(int i=0;  i<framesRead && waveLoading < frames;  i++) {
                        for(int c=0;  c<channels; c++) {
                            int s = ((short)buffer[ndx+1] << 8) + ((short)buffer[ndx] & 0xff);
                            ndx += 2;
                            wave[c][waveLoading] = s * (1.0f / 32767.0f);
                        }
                        
                        waveLoading++;
                    } 
                }
            }
            
                    
            // Done with the stream
            stream.close();
        
        } catch (IOException ex) {
            throw new AudibleException("I/O Exception reading audio " + source);
        }
        

            
        notifyUsers();
    }

    /**
     * @return the wave
     */
    public float[][] getWave() {
        return wave;
    }
    
    
    
    /**
     * Set the number of audio channels. Can only be called before 
     * the audio array is allocated.
     * @param channels the channels to set
     */
    public void setChannels(int channels) {
        assert wave == null;
        this.channels = channels;
        notifyUsers();
    }

    /**
     * @return the channels
     */
    public int getChannels() {
        return channels;
    }

    /**
     * @return the frames
     */
    public int getFrames() {
        return frames;
    }

    
    /**
     * @param nativeFrequency the nativeFrequency to set
     */
    public void setNativeFrequency(float nativeFrequency) {
        this.nativeFrequency = nativeFrequency;
        notifyUsers();
    }
    
    /**
     * @return the nativeFrequency
     */
    public float getNativeFrequency() {
        return nativeFrequency;
    }

    /**
     * Add a user to the collection of 
     * users of this wave table.
     * @param user User to add
     */
    public void addUser(User user) {
        if(!users.contains(user)) {
            users.add(user);
        }
    }
    
    /**
     * Remove a user from the collection of
     * users of this wave table.
     * @param user 
     */
    public void removeUser(User user) {
        users.remove(user);
    }
    
    /**
     * Tell all users that the wavetable has changed.
     */
    private void notifyUsers() {
        for(User user : users ) {
            user.waveTableChanged();
        }
    }
}
