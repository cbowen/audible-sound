/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.msu.cse.audible.sound;

/**
 * Exception class for the AudibleSound class
 * @author cbowen
 */
public class AudibleException extends Exception {
    
    /**
	 * Serialization ID for this exception
	 */
	private static final long serialVersionUID = 5693953127916081569L;

	public AudibleException(String message) {
        super(message);
    }
}
