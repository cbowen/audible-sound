package edu.msu.cse.audible.sound;

/**
 * This class represents an audio voice. It is the base class and 
 * all actual voices are derived from it.
 */
public abstract class Voice {
   
    /**
     * Every Audible component is associated with an Audible
     * object that maintains information such as the sample rate.
     */
    protected final Audible audible;
    
    /**
     * This is where the generated audio samples are store.
     */
    protected final float[] audio = new float[2];
    
    /** 
     * The current frame for this object. This value gets incremented
     * once per frame so we know when a new frame arrives.
     */
    protected int currentFrame = -1;

    
    /**
     * Constructor
     * @param audible Audible system we are a member of
     */
    public Voice(Audible audible) {
        this.audible = audible;
    }
    
    /**
     * Indicate that a new sample frame is required.
     * This is called each time a new sample frame is to be
     * generated. It will call update only if called with a 
     * new value of f. This means this function can be 
     * called multiple times with the same frame number and
     * not update extra amounts.
     * 
     * @param f frame number.
     */
    public synchronized void frame(int f) {
        if(f != currentFrame) {
            // This is a new frame
            currentFrame = f;
            update(audible.getSamplePeriod());
        }
    }
    
    /**
     * Advance the current time by one frame.
     * 
     * @param delta The frame/sample period
     */
    public abstract void update(float delta);
    
    /**
     * Get the audio frame.
     * @return 
     */
    public synchronized float[] get() {
        return audio;
    }
    
    /**
     * Is this voice done?
     * @return true if the voice is done
     */
    public boolean isDone() {
        return false;
    }
}
