package edu.msu.cse.audible.sound;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * This class creates a queue of voices that are played one after
 * the other. This class does not generate an end event.
 */
public class QueuedVoices extends Filter {
    
    /**
     * An empty listener is called when the queue becomes 
     * completely empty.
     */
    public interface Listener {
        void filled();
        void empty();
        void cleared();
        void silent(float duration);
    }
    
    /** The queue of pending voices */
    private final LinkedList<Voice> queue = new LinkedList<>();
    
    public ArrayList<Listener> listeners = new ArrayList<>();
    
    /** How often any notify listener is called */
    private final float notifyInterval = 0.1f;
    
    /** Keep track of how long we have been silent */
    private float silentPeriod  = 0;
    
    private float silentDuration = 0;
    
    /**
     * Constructor
     * @param audible Audible object we are a member of
     */
    public QueuedVoices(Audible audible) {
        super(audible); 
    }
    
    /**
     * Update the filter in time.
     * @param delta Amount of time since last call to update
     */
    @Override
    public synchronized void update(float delta) {        
        // Has the current source ended?
        if(source != null) {
            if(source.isDone()) {
                source = null;
                if(!queue.isEmpty()) {
                    source = queue.removeFirst();
                } else {
                    for(Listener listener : listeners) {
                        listener.empty();
                    }
                }
            }
        }
        
        // Get audio from the current source if there is one
        if(source != null) { 
            float [] sourceAudio = source.get();
           
           audio[0] = sourceAudio[0];
           audio[1] = sourceAudio[1];
        } else {
            audio[0] = audio[1] = 0;
            
            silentPeriod += delta;
            silentDuration += delta;
            if(silentPeriod >= notifyInterval) {
                silentPeriod -= notifyInterval;
                
                for(Listener listener : listeners) {
                    listener.silent(silentDuration);
                }
            }
        }
    }
    
    /**
     * Add a voice to the queue
     * @param voice Voice to add
     */
    public synchronized void add(Voice voice) {
        if(source == null) {
            source = voice;
            
            silentPeriod = 0;
            silentDuration = 0;
            for(Listener listener : listeners) {
                listener.filled();
            }
                                
        } else {
            queue.add(voice);
        }
    }
    
    /**
     * Add voice to the queue, removing any other queued voices
     * except what is currently actively playing.
     * @param voice Voice to add
     */
    public synchronized void onlyPending(Voice voice) {
        queue.clear();
        add(voice);
    }
    
    /**
     * Clear the queue completely.
     */
    public synchronized void clear() {
        queue.clear();
        source = null;
        
        for(Listener listener : listeners) {
            listener.cleared();
        }
    }
    
    /**
     * Clear what is pending in the queue
     * 
     * This only clears future audio, not the current audio
     */
    public synchronized void clearPending() {
        queue.clear();
    }
    
    public boolean isEmpty() {
        return source == null && queue.isEmpty();
    }
    
    /**
     * Return the total size of the queue plus any current playing voice
     * @return Size
     */
    public int size() {
        return (source != null ? 1 : 0) + queue.size();
    }
    
    public void addEmptyListener(Listener listener) {
        listeners.add(listener);
    }
    
    public void removeEmptyListener(Listener listener) {
        listeners.remove(listener);
    }
}
