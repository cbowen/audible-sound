package edu.msu.cse.audible.sound;

import edu.msu.cse.audible.speech.AudibleSpeechException;
import edu.msu.cse.audible.speech.AudibleSpeechFactoryBase;

/**
 * This is a factory class that is derived from the specific speech
 * support factory and generates SpeechVoice objects with given spoken
 * content.
 */
public class AudibleSpeechFactory extends AudibleSpeechFactoryBase {
    
    private final Audible audible;
    
    /** Threshold for when we determine the audio starts and ends */
    private float threshold = 0.005f;
    
    public AudibleSpeechFactory(Audible audible) {
        super();
        this.audible = audible;
    }
    
    public SpeechVoice speak(String text) {
        float [] samples;
        
        try {
            samples = speechToAudio(text);
        } catch (Exception ex) {
            return null;
        }
        
        //
        // The output of FreeTTS has lots of space on the front and
        // back of every utterance. This trims that space off. 
        //
        int start = 1;
        int end = samples.length - 1;
        
        // Trim off the front
        for( ; start < end;  start++) {
            if(samples[start] > threshold) {
                start--;
                break;
            }
        }
        
        // Trim off the end 
        for( ; end > start;  end--) {
            if(samples[end] > threshold) {
                end++;
                break;
            }
        }      
        
        return new SpeechVoice(audible, samples, start, end, getSampleRate());
    }
    
    public void open() throws AudibleException {
        try {
            openSpeech();
        } catch(AudibleSpeechException ex) {
            throw new AudibleException("Unable to open speech system: " + ex.getMessage());
        }
    }
    
    public void close() throws AudibleException {
        try {
            closeSpeech();
        } catch(AudibleSpeechException ex) {
            throw new AudibleException("Unable to close speech system: " + ex.getMessage());
        }
    }
}
