/**
 * The AudibleSound library provides classes that simplify
 * the generation and manipulation of synthesized sound
 * and voice.
 * @author Charles B. Owen
 * @version 1.00 02-19-2014 Declared version number
 */
package edu.msu.cse.audible.sound;
