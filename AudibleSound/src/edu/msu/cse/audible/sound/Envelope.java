package edu.msu.cse.audible.sound;

/**
 * This audio filter creates a simple envelope that allows for
 * linear changes in amplitude that can be sequenced.
 */
public class Envelope extends Filter {
    
    /** The interpolation sequencer than runs the amplitude changes */
    private InterpolateSequence sequencer = new InterpolateSequence();
    
    /**
     * Constructor
     * @param audible Audible object we are a member of
     */
    public Envelope(Audible audible) {
        super(audible); 
    }
        
    /**
     * Constructor
     * @param audible Audible object we are a member of
     * @param source A source that is automatically installed.
     */
    public Envelope(Audible audible, Voice source) {
        super(audible, source);
    }
    
    public void clear() {
        sequencer.clear();
        sequencer.setValue(0);
    }
    
    /**
     * Update the filter in time.
     * @param delta Amount of time since last call to update
     */
    @Override
    public void update(float delta) {
        sequencer.update(delta);
        
        if(source != null) {
           float [] sourceAudio = source.get();
           float gain = sequencer.getValue();
           
           audio[0] = sourceAudio[0] * gain;
           audio[1] = sourceAudio[1] * gain;
        } else {
            audio[0] = audio[1] = 0;
        }
    }
    
    /**
     * Reset for reuse after any done has been hit
     */
    public void reset() {
        sequencer.reset();
    }
    
    /**
     * Is this sequence done? A done condition is indicated by
     * hitting an end event in the sequence or if a source is done.
     * @return true if an end event has been hit
     */
    public boolean isDone() {
        return sequencer.isDone() || (source != null && source.isDone());
    }

    /**
     * Create a linear interpolation sequence.
     * @param target Target amplitude, normally 0 to 1
     * @param duration Amount of time to reach target
     */
    public void linear(float target, float duration) {
        sequencer.linear(target, duration);
    }
    
    /**
     * Create a delay event in the sequence.
     * @param duration Amount of delay
     */
    public void delay(float duration) {
        sequencer.delay(duration);
    }
    
    /**
     * Create an end event in the sequence.
     */
    public void end() {
        sequencer.end();
    }
    
    /**
     * Create an attack/release sequence. The total duration of 
     * the sequence is in duration. The attack and release times are
     * considered part of the sequence. Does not create an end event!
     * @param gain Gain at full amplitude
     * @param attack Attack time in seconds
     * @param duration Total sequence duration in seconds
     * @param release Release time in seconds
     */
    public void ar(float gain, float attack, float duration, float release) {
        sequencer.linear(gain, attack);
        duration -= (attack + release);
        if(duration > 0) {
            sequencer.delay(duration);
        }
        sequencer.linear(0, release);
    }
}
