package edu.msu.cse.audible.sound;

import java.util.Hashtable;

/**
 * This class converts midi notes to frequencies.
 */
public class Notes {
    
    private static Notes notes = null;
    
    private Hashtable<String, Float> lookup = new Hashtable<String, Float>();
    
    /**
     * Singleton class getter.
     * @return 
     */
    public static Notes get() {
        if(notes == null) {
            notes = new Notes();
        }

        return notes;
    }
    
    /**
     * Convert a MIDI note name into a frequency.
     * @param note Name of the note
     * @return Frequence or zero if not found
     */
    public static float noteToFreq(String note) {
        Notes n = get();
        
        Float f = n.lookup.get(note);
        if(f == null) {
            return 0;
        }
        
        return f;
    }
    
    public Notes() {
        lookup.put("A0", 27.5f);
        lookup.put("A#0", 29.1352f);
        lookup.put("Bb0", 29.1352f);
        lookup.put("B0", 30.8677f);
        lookup.put("C1", 32.7032f);
        lookup.put("C#1", 34.6478f);
        lookup.put("Db1", 34.6478f);
        lookup.put("D1", 36.7081f);
        lookup.put("D#1", 38.8909f);
        lookup.put("Eb1", 38.8909f);
        lookup.put("E1", 41.2034f);
        lookup.put("F1", 43.6535f);
        lookup.put("F#1", 46.2493f);
        lookup.put("Gb1", 46.2493f);
        lookup.put("G1", 48.9994f);
        lookup.put("G#1", 51.9131f);
        lookup.put("Ab1", 51.9131f);
        lookup.put("A1", 55.0f);
        lookup.put("A#1", 58.2705f);
        lookup.put("Bb1", 58.2705f);
        lookup.put("B1", 61.7354f);
        lookup.put("C2", 65.4064f);
        lookup.put("C#2", 69.2957f);
        lookup.put("Db2", 69.2957f);
        lookup.put("D2", 73.4162f);
        lookup.put("D#2", 77.7817f);
        lookup.put("Eb2", 77.7817f);
        lookup.put("E2", 82.4069f);
        lookup.put("F2", 87.3071f);
        lookup.put("F#2", 92.4986f);
        lookup.put("Gb2", 92.4986f);
        lookup.put("G2", 97.9989f);
        lookup.put("G#2", 103.826f);
        lookup.put("Ab2", 103.826f);
        lookup.put("A2", 110.0f);
        lookup.put("A#2", 116.541f);
        lookup.put("Bb2", 116.541f);
        lookup.put("B2", 123.471f);
        lookup.put("C3", 130.813f);
        lookup.put("C#3", 138.591f);
        lookup.put("Db3", 138.591f);
        lookup.put("D3", 146.832f);
        lookup.put("D#3", 155.563f);
        lookup.put("Eb3", 155.563f);
        lookup.put("E3", 164.814f);
        lookup.put("F3", 174.614f);
        lookup.put("F#3", 184.997f);
        lookup.put("Gb3", 184.997f);
        lookup.put("G3", 195.998f);
        lookup.put("G#3", 207.652f);
        lookup.put("Ab3", 207.652f);
        lookup.put("A3", 220.0f);
        lookup.put("A#3", 233.082f);
        lookup.put("Bb3", 233.082f);
        lookup.put("B3", 246.942f);
        lookup.put("C4", 261.626f);
        lookup.put("C#4", 277.183f);
        lookup.put("Db4", 277.183f);
        lookup.put("D4", 293.665f);
        lookup.put("D#4", 311.127f);
        lookup.put("Eb4", 311.127f);
        lookup.put("E4", 329.628f);
        lookup.put("F4", 349.228f);
        lookup.put("F#4", 369.994f);
        lookup.put("Gb4", 369.994f);
        lookup.put("G4", 391.995f);
        lookup.put("G#4", 415.305f);
        lookup.put("Ab4", 415.305f);
        lookup.put("A4", 440.0f);
        lookup.put("A#4", 466.164f);
        lookup.put("Bb4", 466.164f);
        lookup.put("B4", 493.883f);
        lookup.put("C5", 523.251f);
        lookup.put("C#5", 554.365f);
        lookup.put("Db5", 554.365f);
        lookup.put("D5", 587.33f);
        lookup.put("D#5", 622.254f);
        lookup.put("Eb5", 622.254f);
        lookup.put("E5", 659.255f);
        lookup.put("F5", 698.456f);
        lookup.put("F#5", 739.989f);
        lookup.put("Gb5", 739.989f);
        lookup.put("G5", 783.991f);
        lookup.put("G#5", 830.609f);
        lookup.put("Ab5", 830.609f);
        lookup.put("A5", 880.0f);
        lookup.put("A#5", 932.328f);
        lookup.put("Bb5", 932.328f);
        lookup.put("B5", 987.767f);
        lookup.put("C6", 1046.5f);
        lookup.put("C#6", 1108.73f);
        lookup.put("Db6", 1108.73f);
        lookup.put("D6", 1174.66f);
        lookup.put("D#6", 1244.51f);
        lookup.put("Eb6", 1244.51f);
        lookup.put("E6", 1318.51f);
        lookup.put("F6", 1396.91f);
        lookup.put("F#6", 1479.98f);
        lookup.put("Gb6", 1479.98f);
        lookup.put("G6", 1567.98f);
        lookup.put("G#6", 1661.22f);
        lookup.put("Ab6", 1661.22f);
        lookup.put("A6", 1760.0f);
        lookup.put("A#6", 1864.66f);
        lookup.put("Bb6", 1864.66f);
        lookup.put("B6", 1975.53f);
        lookup.put("C7", 2093.0f);
        lookup.put("C#7", 2217.46f);
        lookup.put("Db7", 2217.46f);
        lookup.put("D7", 2349.32f);
        lookup.put("D#7", 2489.02f);
        lookup.put("Eb7", 2489.02f);
        lookup.put("E7", 2637.02f);
        lookup.put("F7", 2793.83f);
        lookup.put("F#7", 2959.96f);
        lookup.put("Gb7", 2959.96f);
        lookup.put("G7", 3135.96f);
        lookup.put("G#7", 3322.44f);
        lookup.put("Ab7", 3322.44f);
        lookup.put("A7", 3520.0f);
        lookup.put("A#7", 3729.31f);
        lookup.put("Bb7", 3729.31f);
        lookup.put("B7", 3951.07f);
        lookup.put("C8", 4186.01f);     
    }
}
