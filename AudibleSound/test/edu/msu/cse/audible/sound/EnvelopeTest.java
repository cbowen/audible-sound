/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.msu.cse.audible.sound;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author cbowen
 */
public class EnvelopeTest extends ChannelUser {
    
    @Before
    public void setUp() throws AudibleException {
        super.setUp();
    }
    
    @After
    public void tearDown() throws AudibleException {
        super.tearDown();
    }

    @Test
    public void test1() {
        System.out.println("* EnvelopeTest: initial test");
        SineVoice voice = new SineVoice(audible);
        voice.setAmplitude(1);
        Envelope envelope = new Envelope(audible, voice);
        channel.addVoice(envelope);
        
        envelope.linear(0.2f, 1);
        envelope.delay(1);
        envelope.linear(0, 1);
        envelope.end();
        
        try {
            Thread.sleep(3500);
        } catch (InterruptedException ex) {
        }
        
        assertFalse("Channel removed itself when done", channel.hasVoice(envelope));
    }
    
    @Test
    public void test2() {
        System.out.println("* EnvelopeTest: ar");
        SineVoice voice = new SineVoice(audible);
        voice.setAmplitude(1);
        Envelope envelope = new Envelope(audible, voice);
        channel.addVoice(envelope);
        
        envelope.ar(0.2f, 0.1f, 1, 0.1f);
        envelope.end();
        
        try {
            Thread.sleep(1500);
        } catch (InterruptedException ex) {
        }
        
        assertFalse("Channel removed itself when done", channel.hasVoice(envelope));
    }

}