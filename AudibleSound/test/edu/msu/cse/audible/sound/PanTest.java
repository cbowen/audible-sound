/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.msu.cse.audible.sound;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author cbowen
 */
public class PanTest extends ChannelUser {
    
    @Before
    public void setUp() throws AudibleException {
        super.setUp();
    }
    
    @After
    public void tearDown() throws AudibleException {
        super.tearDown();
    }

    @Test
    public void test1() {
        System.out.println("* PanTest: initial test");
        SineVoice voice = new SineVoice(audible);
        voice.setAmplitude(0.2f);
        Pan pan = new Pan(audible, voice);
        channel.addVoice(pan);
        
        pan.linear(-1, 1);
        pan.delay(1);
        pan.linear(1, 2);
        pan.delay(1);
        pan.linear(0, 1);
        pan.end();
        
        try {
            Thread.sleep(7000);
        } catch (InterruptedException ex) {
        }
        
        assertFalse("Channel removed itself when done", channel.hasVoice(pan));
    }
    
    @Test
    public void test2() {
        System.out.println("* PanTest: left and right set");
        SineVoice voice = new SineVoice(audible);
        voice.setAmplitude(0.2f);
        Pan pan = new Pan(audible, voice);
        channel.addVoice(pan);
        
        pan.set(-1);
        
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
        }
        
        pan.set(1);
        
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
        }
        
        assertTrue("Channel still there when done", channel.hasVoice(pan));
    }
    

}