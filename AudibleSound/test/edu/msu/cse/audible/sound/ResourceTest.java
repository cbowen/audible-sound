package edu.msu.cse.audible.sound;

import java.io.IOException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cbowen
 */
public class ResourceTest extends ChannelUser {
    
    @Before
    public void setUp() throws AudibleException {
        super.setUp();
    }
    
    @After
    public void tearDown() throws AudibleException {
        super.tearDown();
    }

    @Test
    public void test1() {
        System.out.println("* Resource Test - ensure we can access resource files");
        URL url = edu.msu.cse.audible.sound.resources.Resources.class.getResource("c4.wav");
        AudioInputStream stream = null;
        try {
            stream = AudioSystem.getAudioInputStream(url);
            
            AudioFormat format = stream.getFormat();
            int size = (int)(stream.getFrameLength() * format.getFrameSize());
            byte [] buffer = new byte[size];
            stream.read(buffer);
            
            
            int xx = 0;
        } catch (UnsupportedAudioFileException ex) {
            fail("Unsupported audio file exception");
        } catch (IOException ex) {
            fail("IO Exception");
        }
        

    }
    
    
}