/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.msu.cse.audible.sound;

import edu.msu.cse.audible.speech.AudibleSpeechFactoryBase;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author cbowen
 */
public class AudibleSpeechFactoryTest extends ChannelUser {
    
    /** The FreeTTS engine does not like to be destroyed and 
     * recreated, so I only create it once for all testing.
     */
    private static AudibleSpeechFactory factory = null;
    
    public AudibleSpeechFactoryTest() {
    }
    
    @Before
    public void setUp() throws AudibleException {
        super.setUp();
        
        if(factory == null) {
            factory = new AudibleSpeechFactory(audible);
            factory.setPreferredVoiceName("kevin16");
            factory.open();
        }
    }
    
    @After
    public void tearDown() throws AudibleException {
        super.tearDown();

        
    }

    @Test
    public void test1() {
        System.out.println("* AudibleSpeechFactoryTest: saying something");
                
        SpeechVoice voice = factory.speak("This is a test of the audible speech factory");
        channel.addVoice(voice);
       
        try {
            Thread.sleep(3000);
        } catch (InterruptedException ex) {
        }
        
        System.out.println("* AudibleSpeechFactoryTest: testing for voice removal");
        assertFalse("Should have removed itself when done talking", channel.hasVoice(voice));
    }
    
    @Test
    public void test2() {
        System.out.println("* AudibleSpeechFactoryTest: sequential talking");
                
        QueuedVoices voice = new QueuedVoices(audible);
        channel.addVoice(voice);
        
        SpeechVoice voice1 = factory.speak("one");
        voice.add(voice1);
        SpeechVoice voice2 = factory.speak("two");
        voice.add(voice2);
        SpeechVoice voice3 = factory.speak("three");
        voice.add(voice3);
        SpeechVoice voice4 = factory.speak("four");
        voice.add(voice4);

       
        try {
            Thread.sleep(3000);
        } catch (InterruptedException ex) {
        }
        
        channel.removeVoice(voice);
    }
}