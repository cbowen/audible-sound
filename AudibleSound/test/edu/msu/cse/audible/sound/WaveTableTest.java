/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.msu.cse.audible.sound;

import java.net.URL;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author cbowen
 */
public class WaveTableTest extends ChannelUser {
    
    @Before
    public void setUp() throws AudibleException {
        super.setUp();
    }
    
    @After
    public void tearDown() throws AudibleException {
        super.tearDown();
    }

    @Test
    public void test1() throws AudibleException {
        System.out.println("* WaveTableTest: C4.wav, native");
        URL url = edu.msu.cse.audible.sound.resources.Resources.class.getResource("c4.wav");
        
        WaveTable wave = new WaveTable(audible);
        WaveTableVoice voice = new WaveTableVoice(audible, wave);
        wave.loadFromResource(url);

        channel.addVoice(voice);
        
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
        }
        
        channel.removeVoice(voice);      
    }
    
    @Test
    public void test2() throws AudibleException {
        String file = "hammond_jazz_c4_loop.wav";
        System.out.println("* WaveTableTest: " + file + ", up an octave");
        URL url = edu.msu.cse.audible.sound.resources.Resources.class.getResource(file);
        
        WaveTableVoice voice = new WaveTableVoice(audible);
        voice.loadFromResource(url);
        voice.setPlayFrequency(2);
        channel.addVoice(voice);
        
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
        }
        
        channel.removeVoice(voice);      
    }

//    @Test
//    public void test3() throws AudibleException {
//        System.out.println("* WaveTableTest: C4.wav, looping");
//        URL url = edu.msu.cse.audible.sound.resources.Resources.class.getResource("c4.wav");
//        
//        WaveTable voice = new WaveTable(audible);
//        voice.loadFromResource(url);
//        voice.setLooping(true);
//        
//        channel.addVoice(voice);
//        
//        try {
//            Thread.sleep(4000);
//        } catch (InterruptedException ex) {
//        }
//        
//        channel.removeVoice(voice);      
//    }
    
    @Test
    public void test4() throws AudibleException {
        //String file = "glottal_c4.wav";
        String file = "hammond_jazz_c4_loop.wav";
        System.out.println("* WaveTableTest: " + file);
        URL url = edu.msu.cse.audible.sound.resources.Resources.class.getResource(file);
        
        WaveTable wave = new WaveTable(audible);
        WaveTableVoice voice = new WaveTableVoice(audible, wave);
        wave.loadFromResource(url);
        voice.setLooping(true);
        voice.setPlayFrequency(1/1.059463f);
        
        channel.addVoice(voice);
        
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
        }
        
        channel.removeVoice(voice);      
    }
    
    
}