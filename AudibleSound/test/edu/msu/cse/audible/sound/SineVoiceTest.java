/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.msu.cse.audible.sound;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cbowen
 */
public class SineVoiceTest extends ChannelUser {
    
    @Before
    public void setUp() throws AudibleException {
        super.setUp();
    }
    
    @After
    public void tearDown() throws AudibleException {
        super.tearDown();
    }

    @Test
    public void test1() {
        System.out.println("* SineVoiceTest: default frequecy and 0.2 amplitude");
        SineVoice voice = new SineVoice(audible);
        voice.setAmplitude(0.2f);
        channel.addVoice(voice);
        
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
        }
        
        channel.removeVoice(voice);      
    }
    
    
    @Test
    public void test2() {
        System.out.println("* SineVoiceTest: 1000Hz, 0.4 amplitude");
        SineVoice voice = new SineVoice(audible);
        voice.setAmplitude(0.4f);
        voice.setFrequency(1000);
        channel.addVoice(voice);
        
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
        }
        
        channel.removeVoice(voice);      
    }
    
        
    @Test
    public void test3() {
        System.out.println("* SineVoiceTest: G5, 0.4 amplitude");
        SineVoice voice = new SineVoice(audible);
        voice.setAmplitude(0.4f);
        voice.setNote("G5");
        channel.addVoice(voice);
        
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
        }
        
        channel.removeVoice(voice);      
    }
    
    @Test
    public void test4() {
        System.out.println("* SineVoiceTest: 1000Hz, 0.1 amplitude");
        SineVoice voice = new SineVoice(audible);
        voice.setAmplitude(0.1f);
        voice.setFrequency(1000);
        channel.addVoice(voice);
        
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
        }
        
        channel.removeVoice(voice);      
    }
    
}