package edu.msu.cse.audible.sound;

import static org.junit.Assert.assertTrue;

/**
 *
 * @author cbowen
 */
public class ChannelUser {
    protected Audible audible = null;
    
    protected AudibleChannel channel = null;
    
    public void setUp() throws AudibleException {
        audible = Audible.createAudible();
        
        channel = audible.createChannel();
        assertTrue("Is member of audible", audible.hasChannel(channel));
        assertTrue("Audible is owner", channel.getAudible() == audible);
        
        channel.open();
    }
    
    public void tearDown() throws AudibleException {
        channel.close();
    } 
}
