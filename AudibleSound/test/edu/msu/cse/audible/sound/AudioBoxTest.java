package edu.msu.cse.audible.sound;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * This test checks to see if an AudioBox 1818VSL is attached
 * to the system. If so, it sends output to all channels to 
 * test that we can write to this device.
 * @author Charles B. Owen
 */
public class AudioBoxTest {
    
    private static final String[] channelNames = {"Line Out 1/2 (AudioBox 1818VSL Audio)",
        "Line Out 3/4 (AudioBox 1818VSL Audio)",
        "Line Out 5/6 (AudioBox 1818VSL Audio)",
        "Phones 7/8 (AudioBox 1818VSL Audio)"
    };
    
    public AudioBoxTest() {
    }
    
    @Before
    public void setUp() {

    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testAudioBox() throws AudibleException {
        Audible audible = null;
        try {
            audible = Audible.createAudible();
        } catch (AudibleException ex) {
            fail("Failed to set up AudibleSound - " + ex.getMessage());
        }
        
        List<String> names = audible.getMixerNames();
        boolean got = false;
        for(String name : names) {
            if(name.contains("AudioBox 1818VSL")) {
                got = true;
                break;
            }
        }
        
        if(!got) {
            System.out.println("No AudioBox 1818VSL, aborting test");
            return;
        }
        
        System.out.println("Found AudioBox 1818VSL");
        
        AudibleChannel[] channels = new AudibleChannel[4];
        
        for(int i=0;  i<4;  i++) {
            AudibleChannel channel = audible.createChannel();
            channels[i] = channel;
            try {
                channel.open(channelNames[i]);
            } catch (AudibleException ex) {
                fail("Failed to open channel " + channelNames[i]);
            }
            
            System.out.println("* SineVoiceTest: 1000Hz, 0.4 amplitude");
            SineVoice voice = new SineVoice(audible);
            voice.setAmplitude(0.4f);
            voice.setFrequency(1000);
            channel.addVoice(voice);



            //channel.removeVoice(voice);
        }
             
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
        }  
        
        for(int i=0; i<4;  i++) {
            channels[i].close();
        }

        
    }
}