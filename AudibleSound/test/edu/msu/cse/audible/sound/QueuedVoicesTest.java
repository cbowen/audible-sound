/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.msu.cse.audible.sound;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author cbowen
 */
public class QueuedVoicesTest extends ChannelUser {
    
    @Before
    public void setUp() throws AudibleException {
        super.setUp();
    }
    
    @After
    public void tearDown() throws AudibleException {
        super.tearDown();
    }

    
    @Test
    public void test2() {
        System.out.println("* QueuedVoicesTest: two");
        QueuedVoices queue = new QueuedVoices(audible);
        channel.addVoice(queue);
        
        // Delay to see how it works with nothing queued
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
        }
        
        //
        // Voice 1
        //
        
        SineVoice voice1 = new SineVoice(audible);
        voice1.setAmplitude(1);
        voice1.setFrequency(440);
        Envelope envelope1 = new Envelope(audible, voice1);
         
        envelope1.ar(0.2f, 0.1f, 1, 0.1f);
        envelope1.end();
        
        queue.add(envelope1);

        //
        // Voice 2
        //
        
        SineVoice voice2 = new SineVoice(audible);
        voice2.setAmplitude(1);
        voice2.setFrequency(880);
        Envelope envelope2 = new Envelope(audible, voice2);
         
        envelope2.ar(0.2f, 0.1f, 1, 0.1f);
        envelope2.end();
        
        queue.add(envelope2);
        
        try {
            Thread.sleep(2500);
        } catch (InterruptedException ex) {
        }
        
        assertTrue("Channel should not remove itself when done", channel.hasVoice(queue));
    }

}