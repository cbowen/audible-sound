/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.msu.cse.audible.sound;

import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * This is a test for only the creating of the Audible class
 * object.
 * @author cbowen
 */
public class AudibleTest {
    
    public AudibleTest() {
    }
    
    @Before
    public void setUp() {

    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod() {
        Audible audible = null;
        try {
            audible = Audible.createAudible();
        } catch (AudibleException ex) {
            fail("Failed to set up AudibleSound - " + ex.getMessage());
        }
        
        List<String> mixers = audible.getMixerNames();
        for(String name : mixers) {
            System.out.println(name);
        }
    }
}