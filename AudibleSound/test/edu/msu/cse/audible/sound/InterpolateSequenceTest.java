/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.msu.cse.audible.sound;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cbowen
 */
public class InterpolateSequenceTest {
    
    public InterpolateSequenceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }


    /**
     * Test of reset method, of class InterpolateSequence.
     */
    @Test
    public void test() {
        System.out.println("Basic test of adding a simple interpolation");
        InterpolateSequence instance = new InterpolateSequence();
        
        assertEquals("Initial value should be zero", 0, instance.getValue(), 0.0001);
        
        instance.update(0.1f);
        assertEquals("No change after update", 0, instance.getValue(), 0.0001);
        
        instance.linear(0.7f, 2);
        assertEquals("Should be zero pending after adding one", 0, instance.numPending());
        assertEquals("Should be one active", true, instance.isActive());
        
        for(int i=0;  i<200;  i++) {
            instance.update(0.01f);
            assertEquals("Intepolated value", 0.7f * (i + 1) / 200.0f, instance.getValue(), 0.0001);
            //System.out.println("Value = " + instance.getValue());
        }
        
        instance.update(0.001f);
        assertEquals("Intepolated value", 0.7f, instance.getValue(), 0.0001);

        // Test that the event is cleared.
        assertEquals("Should be zero pending", 0, instance.numPending());
        assertEquals("Should be none active", false, instance.isActive());
    }
    
    /**
     * Test of reset method, of class InterpolateSequence.
     */
    @Test
    public void test2() {
        System.out.println("Test with two interpolations");
        InterpolateSequence instance = new InterpolateSequence();
        
        assertEquals("Initial value should be zero", 0, instance.getValue(), 0.0001);
        
        instance.update(0.1f);
        assertEquals("No change after update", 0, instance.getValue(), 0.0001);
        
        float time = 0;
        
        instance.linear(0.7f, 2);
        instance.delay(1);
        instance.linear(0, 2);
        instance.end();
        assertEquals("Should be two pending after adding four", 3, instance.numPending());
        assertEquals("Should be one active", true, instance.isActive());
        
        for(int i=0;  i<55;  i++) {
            instance.update(0.1f);
            time += 0.1;
            
            //assertEquals("Intepolated value", 0.7f * (i + 1) / 200.0f, instance.getValue(), 0.0001);
            System.out.println("Value = " + instance.getValue());

            assertEquals("Done correct?", time > 4.99, instance.isDone());
        }
        
        instance.update(0.001f);
        assertEquals("Intepolated value", 0, instance.getValue(), 0.0001);

        // Test that the event is cleared.
        assertEquals("Should be zero pending", 0, instance.numPending());
        assertEquals("Should be none active", false, instance.isActive());
        assertEquals("Should be done", true, instance.isDone());
    }

}